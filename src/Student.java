import java.util.Comparator;
import java.util.Objects;

public class Student {
  /**
   * Student.
   */
  private String name;
  private int grade;

  public Student(String name, int grade) {
    this.name = name;
    this.grade = grade;
  }

  public static Comparator<Student> byGrade() {
    return new Comparator<Student>() {
      @Override
      public int compare(Student o1, Student o2) {
        return o1.grade - o2.grade;
      }
    };
  }

  @Override
  public String toString() {
    return "{" + name + "," + grade + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Student student = (Student) o;
    return grade == student.grade && Objects.equals(name, student.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, grade);
  }

  public String getName() {
    return  name;
  }
}