/**
 * Unique.
 */
public interface Unique<T> {

  void unique();
}
