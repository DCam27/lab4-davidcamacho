import java.util.*;

/**
 * UniArrayList.
 */
public class UniArrayList<E> implements List<E>, Sortable<E>, Unique<E> {

    private Object[] elements;
    private int size;

    public UniArrayList() {
        this.elements = new Object[0];
        this.size = 0;
    }

    public UniArrayList(E[] array) {
        this.size = array.length;
        this.elements = new Object[array.length];

        for (int i = 0; i < array.length; i++) {
            this.elements[i] = array[i];
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(elements[i]);
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean add(E e) {
        if (size == elements.length) {
            elements = resizeArray(elements, size + 1000);

        }
        elements[size] = e;
        size++;
        return true;
    }

    @Override
    public void add(int index, E element) {

        if (size == elements.length) {
            elements = resizeArray(elements, size + 1);

        }
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        for (int i = size - 1; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    private Object[] resizeArray(Object[] array, int newSize) {
        Object[] newArray = new Object[newSize];
        for (int i = 0; i < Math.min(array.length, newSize); i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    @Override
    public void sort() {
        for (int i = 1; i < size; i++) {
            E key = (E) elements[i];
            int j = i - 1;

            while (j >= 0 && ((Comparable<String>) ((Student) key).getName())
                    .compareTo(((Student) elements[j]).getName()) < 0) {
                elements[j + 1] = elements[j];
                j--;
            }

            elements[j + 1] = key;
        }
    }

    @Override
    public void sortBy(Comparator<E> comparator) {
        for (int index = 1; index < size; index++) {
            E currentElement = (E) elements[index];
            int previousIndex = index - 1;

            while (previousIndex >= 0 && comparator.compare((E) elements[previousIndex], currentElement) > 0) {
                elements[previousIndex + 1] = elements[previousIndex];
                previousIndex--;
            }
            elements[previousIndex + 1] = currentElement;
        }

    }

    @Override
    public void unique() {
        if (size <= 1) {
            return;
        }
        boolean[] seen = new boolean[size];
        for (int i = 0; i < size - 1; i++) {
            if (!seen[i]) {
                for (int j = i + 1; j < size; j++) {
                    if (!seen[j] && elements[i].equals(elements[j])) {
                        seen[j] = true;
                    }
                }
            }
        }

        int uniqueSize = 0;
        for (int i = 0; i < size; i++) {
            if (!seen[i]) {
                elements[uniqueSize++] = elements[i];
            }
        }

        size = uniqueSize;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return (E) elements[currentIndex++];
            }
        };
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }


    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (Objects.equals(o, elements[i])) {

                for (int j = i; j < size - 1; j++) {
                    elements[j] = elements[j + 1];
                }
                elements[--size] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        return (E) elements[index];
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public E remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        E removedElement = (E) elements[index];
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[--size] = null;
        return removedElement;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}